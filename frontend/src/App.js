// import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Dashboard from "./Dashboard";
import Navbar from "./Navbar";

function App() {
  return (
    //   // <BrowserRouter>
    // {/* <Navbar /> */}
    //     <Routes>
    //       <Route path="/" element={<Dashboard />}></Route>
    //     </Routes>
    //   </BrowserRouter>
    <>
      <Navbar />
      <Dashboard />
    </>
  );
}

export default App;
