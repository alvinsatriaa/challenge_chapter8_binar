export default function Dashboard() {
  return (
    <div className="container">
      <div className="row mt-3">
        <div className="col form-group">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            class="form-control"
            id="username"
            placeholder="username"
          />
        </div>
        <div className="col form-group">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            class="form-control"
            id="email"
            placeholder="user@mail.com"
          />
        </div>
        <div className="col form-group">
          <label htmlFor="experience">Experience</label>
          <input
            type="number"
            class="form-control"
            id="experience"
            placeholder="1000"
          />
        </div>
        <div className="col form-group">
          <label htmlFor="level">Level</label>
          <input
            type="number"
            class="form-control"
            id="level"
            placeholder="1"
          />
        </div>
        <div className="col form-group d-flex align-items-end">
          <button type="button" class="btn btn-light">
            Search
          </button>
        </div>
      </div>

      <div className="row mt-4">
        <div className="col d-flex justify-content-end">
          <button type="button" class="btn btn-success">
            Add Player
          </button>
        </div>
      </div>

      <div className="row mt-2">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Username</th>
              <th scope="col">Email</th>
              <th scope="col">Experience</th>
              <th scope="col">Level</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>User1</td>
              <td>user1@mail.com</td>
              <td>11000</td>
              <td>11</td>
              <td>
                <a href="#">Edit</a>
                <a href="#" className="ml-3 text-danger">
                  Delete
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
  const [filter, setFilter] = useState({
    username: "",
    email: "",
    experience: "",
    lvl: "",
  });

  const [player, setPlayer] = useState([]);

  const handleOnChange = (e) => {
    setFilter({ ...filter, ...{ [e.target.name]: e.target.value } });
  };

  const fetchPlayer = () => {
    axios({
      method: "GET",
      url: "http://localhost:4000/api/v1/players",
    })
      .then((result) => {
        setFilter(result.data.data);
        setPlayer(result.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleFilter = () => {
    axios
      .get("http://localhost:4000/api/v1/players", { params: filter })
      .then((result) => {
        setPlayer(result.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    fetchPlayer();
  }, []);
}
